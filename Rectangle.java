public class Rectangle {

	int length; 
	int width; 
	int innerlength; 
	int innerwidth; 
	char symbol;
	char [][] myShape;

	/*
	 Constructor for non nested rectangles.
	 Sets the length, width and symbol of the rectangle.
	 Calls the method drawMyShape to create the 2d array.
	*/
	public Rectangle(int length, int width, char symbol) {
		this.length = length;
		this.width = width; 
		this.symbol = symbol; 

		drawMyShape();
	}

	/*
	 Constructor for nested rectangles.
	 Calls the method drawMyShape to create the 2d array and if possible calls the method 
	 drawInnerRectangle to add the inner rectangle.
	*/
	public Rectangle(int length, int width, int innerlength, int innerwidth, char symbol) {
		this.length = length;
		this.width = width; 
		this.innerwidth = innerwidth; 
		this.innerlength = innerlength; 
		this.symbol = symbol; 

		drawMyShape();
		if(possibleInner()) {
			drawMyInnerRectangle();
		}
	}

	/*
	 Loops through the 2d array and adds the wanted symbol to the edges of the rectangle. 
	*/
	void drawMyShape() {

		myShape = new char [length][width];

		for(int i = 0; i < length; i++) {
			for(int j = 0; j < width; j++) {

				if(i == 0 || i == length-1) {
					myShape[i][j] = symbol; 
				}

				else if(j == 0 || j == width-1) {
					myShape[i][j] = symbol;
				}
				else {
					myShape[i][j] = ' ';
				}
			}
		}
	}

	/*
	 Starts at [2,2] in the 2d array and draws the edges of the inner .
	*/
	void drawMyInnerRectangle() {
		for(int i = 2; i < innerlength+2; i++) {
			for(int j = 2; j < innerwidth+2; j++) {

				if(i == 2 || i == innerlength+1) {
					myShape[i][j] = symbol; 
				}

				else if(j == 2 || j == innerwidth+1) {
					myShape[i][j] = symbol;
				}

			}
		}
	}

	/*
	 Checks if there is enough space for the inner rectangle.
	 There must be a single character space between inner and outer rectangles, therefore
	 the inner length and width must be at least 4 smaller. 
	*/
	boolean possibleInner() {
		if((innerlength + 4)<= length && (innerwidth+4)<= width) {
			return true;
		}
		return false;
	}


	/*
	 Returns the 2d array
	*/
	public char [][] getRectangle() {
		return myShape;
	}

}