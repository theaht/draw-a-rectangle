import java.util.Scanner;

public class DrawRectangle {

	/*
	 Main method. While loop calls the method getUserInput() 
	 until the user has pressed the key to stop.
	*/
	public static void main(String[] args) {

		DrawRectangle draw = new DrawRectangle();
		boolean wantToContinue = true;

		while(wantToContinue) {

			wantToContinue = draw.getUserInput();

		}		
	}

	/*
	 Method that askes the user for the sizes and symbol of the rectangles to draw.
	 Returns a boolean, false when the user wants to stop the program.
	*/
	public boolean getUserInput() {

		Scanner scanner = new Scanner(System.in);
		int length = 0;
		int width = 0;
		int innerlength = 0;
		int innerwidth = 0; 
		char symbol = ' ';
		boolean cont = true;
		char another = ' ';
		char nested = ' ';
		Rectangle rectangle;
		
		System.out.print("Do you want to draw nested rectangles?(y/n)");
		nested = yesOrNo(scanner);

		System.out.print("Enter length of rectangle: ");
		length = enterNumber(scanner); 

		System.out.print("Enter width of rectangle: ");
		width = enterNumber(scanner); 

		if(nested == 'y') {

			System.out.print("Enter length of inner rectangle: ");
			innerlength = enterNumber(scanner); 

			System.out.print("Enter width of inner rectangle: ");
			innerwidth = enterNumber(scanner); 
		}

		System.out.print("Enter symbol for drawing: ");
		symbol = scanner.next().charAt(0);

		System.out.println("Here is a drawing of the rectangle: ");

		if(nested == 'y') {
			rectangle = new Rectangle(length, width, innerlength, innerwidth, symbol);
		}
		else {
			rectangle = new Rectangle(length, width, symbol);

		}
		printRectangle(rectangle.getRectangle());

		System.out.print("Do you want to draw another?(y/n)");
		another = yesOrNo(scanner);
		
		if(another == 'n') {
			 cont = false;
		}

		return cont;
			
	}

	/*
	 Method takes in a number (length/width) from the user. Continue asking if the 
	 user types in an invalid character (not a numer).
	 Returns the number receieved by the user 
	 */
	public int enterNumber(Scanner scanner) {
		while(!scanner.hasNextInt()) {
			System.out.println ("Please enter a number");
			scanner.next();
		}
		return scanner.nextInt(); 
	}

	/*
	 Method takes in y or n from the user. Continue asking if the 
	 user types in an invalid character.
	 Returns y or n, depending on what the user answered
	 */
	public char yesOrNo(Scanner scanner) {
		char yorn = scanner.next().charAt(0); 
		while (yorn != 'y' && yorn != 'n') {
			System.out.println("Please enter y or n");
			yorn = scanner.next().charAt(0);
		}
		return yorn;
	}

	/*
	 Method that print the rectangle. 
	*/
	public void printRectangle(char [][] rectangle) {
		for(int i = 0; i < rectangle.length; i++) {
			for (int j = 0; j < rectangle[i].length; j++) {
				System.out.print(rectangle[i][j]);
			}
			System.out.println(); 
		}
	}

}